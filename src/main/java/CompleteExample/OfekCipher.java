package CompleteExample;

public class OfekCipher implements Cipher {

    private final int gapSize;
    private final int numberOfRotations;
    LetterRotator letterRotator = new LetterRotator();

    public OfekCipher(int numberOfRotations, int gapSize) {
        this.numberOfRotations = numberOfRotations;
        this.gapSize = gapSize;
    }

    @Override
    public String encrypt(String textToEncrypt) {
        return generateCipherResult(textToEncrypt, this.numberOfRotations);
    }

    @Override
    public String decrypt(String textToDecrypt) {
        return generateCipherResult(textToDecrypt, -this.numberOfRotations);
    }


    private String generateCipherResult(String textToCipher, int numberOfRotations) {
        StringBuilder cipherResult = new StringBuilder();
        for (int i = 0; i < textToCipher.length(); i++) {
            char letter = generateCipherLetter(textToCipher.charAt(i), i%gapSize, numberOfRotations);
            cipherResult.append(letter);
        }
        return cipherResult.toString();
    }

    private char generateCipherLetter(char letterToCipher, int replaceLetter, int numberOfRotations) {
        if(replaceLetter > 0)
            return letterToCipher;
        else
            return letterRotator.rotateLetter(letterToCipher, numberOfRotations);
    }
}
